const express = require('express');
const path = require('path');
const app = express();
let value = 0;

app.use(express.static(path.join(__dirname, 'calc')));

app.get('/value', (_req, res)=> {
    //let value = req.params.value
    res.status(200).json({ value });
})

app.use('/calc/:operand/:figure', (req, res)=> {
    let operand = req.params.operand;
    let figure = +req.params.figure;

    // check if fetch is needed, or I can just write value

    if (operand === '+') {
        value = figure + value;
    }

    if (operand === '-') {
        value = value - figure;

    }

    if (operand === '*') {
        value = figure * value;
    }

    res.status(200).json({ value });
})

app.listen(3001, () => {
    console.log("Server started on port 3001");
})

