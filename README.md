a) What was fun/hard/interesting to do/learn in this Ex
fun - learning of fetch!
Nice exercise in general. Loved it!

b) What would happen if multiple users would use your calculator?

What will happen is that the same value saved in the server will be used by multiple users at the same time. For example, lets say 2 users - user1 and user2 are using that calculator, starting from value box of 0.
user1 has clicked +4, and saved to the server the result of 4. 
user2 at this time, wasn't doing anything, and is still seeing the current value box set to 0 - Although, the value saved in the server has already turned to 4.
When user2 will calculate something, it will consider the value of the value box as +4, as saved in the server - and not as the current value user2 sees - 0.
Therefore, user2 will get wrong result "from his point of view", for any calcultion.